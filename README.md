# xmpp-discord-bridge

xmpp-discord-bridge's goal is to be a highly integrated bridge between XMPP and Discord.

## Features

- Uni-directional bridging of Discord reactions to XMPP: Currently without XEP-0424, but with a "compat" mode
- Bi-directional bridging of media using Webhook embeds and OOB data
- Uni-directional bridging of Discord presence to XMPP
- Puppetering support on the side of XMPP
- Automatic webhook creation
- Uni-directional sync of the Discord channel's topic
- XMPP Avatar spoofing on the Discord side
- Support for proxying media sent via Discord (With [this miniproxy](https://git.polynom.me/PapaTutuWawa/miniproxy))

## Usage

The bridge can be configured via `config.toml` at either the current working directory or `/etc/xmpp-discord-bridge/config.toml`.

```toml
[general]
jid = "discord.server.example" # JID of the server component
secret = "secret" # Component secret
server = "server.example" # Address of the actual XMPP server
port = 5869 # Component port
discord_token "...." # Token of your Discord bot

# If a message is reacted to, send the following to the MUC:
# > [Message content]
# + :someemoji:
reactions_compat = true

# Use the XMPP user's avatar in the webhook. For options, see
# the [avatars] section
relay_xmpp_avatars = false

# When a user is mentioned, also add the display name infront of the message body, e.g.
# SomeUser, PapaTutuWawa: Hello @SomeUser and @PapaTutuWawa
muc_mention_compat = true

# If true, then xmpp-discord-bridge will also make offline Discord users appear in the MUC.
# If they are offline, then they will have a presence of "xa". If false, then offline users
# will leave the MUC.
dont_ignore_offline = true

# If true, when receiving a file to embed in Discord, the bridge will remove the URL from
# the content, leaving only the embed behind. This prevents Discord users who disabled
# link previews from viewing the embed.
remove_url_on_embed = true

# When sending files from Discord to XMPP, proxy the URLS with this template. "<hmac>" and
# and "<url>" will be substituted. Proxy will not be used if not set.
proxy_discord_urls_to = "https://proxy.server.example/proxy/<hmac>/<urls>"

# Secret for the proxy. Only used when the proxy is used.
hmac_secret = "some-secret"

[avatars]
# Path where avatars can be stored
path = "/some/path"

# URL on which the avatars will be available
url = "https://im.server.example/avatars/"

[discord]
[[discord.channels]]
# The ID of the guild
guild = 00000000000

# The ID of the channel to mirror
channel = 000000000

# The MUC to mirror into
muc = "channel@muc.server.example"
```

In order for the bridge to work, it needs to have an affiliation of `owner` before joining.
The virtual users that follow will be automatically added to the room with an affiliation
of `member`.

## License

See `./LICENSE`
