from setuptools import setup, find_packages

setup(
    name = "xmpp_discord_bridge",
    version = "0.1.0",
    url = "https://git.polynom.me/PapaTutuWawa/xmpp-discord-bridge",
    author = "Alexander \"PapaTutuWawa\"",
    author_email = "papatutuwawa <at> polynom.me",
    license = "GPLc3",
    packages = find_packages(),
    install_requires = [
        "requests==2.27.1",
        "slixmpp==1.7.1",
        "nextcord",
        "toml==0.10.2"
    ],
    extra_require = {
        "dev": [
            "black"
        ]
    },
    zip_safe = True,
    entry_points = {
        "console_scripts": [
            "xmpp-discord-bridge = xmpp_discord_bridge.main:main"
        ]
    }
)
