import logging

import nextcord

class DiscordClient(nextcord.Client):
    def __init__(self, xmpp, config):
        intents = nextcord.Intents.default()
        intents.members = True
        intents.presences = True
        intents.messages = True
        intents.reactions = True

        nextcord.Client.__init__(self, intents=intents)

        self._xmpp = xmpp
        self._config = config
        self._logger = logging.getLogger("discord.client")

    async def on_ready(self):
        await self._xmpp.on_discord_ready()

    async def on_message(self, message):
        await self._xmpp.on_discord_message(message)

    async def on_member_update(self, before, after):
        await self._xmpp.on_discord_member_update(before, after)

    async def on_member_join(self, member):
        await self._xmpp.on_discord_member_join(member)

    async def on_member_leave(self, member):
        await self._xmpp.on_discord_member_leave(member)

    async def on_guild_channel_update(self, before, after):
        await self._xmpp.on_discord_channel_update(before, after)

    async def on_reaction(self, payload):
        message = await (self.get_guild(payload.guild_id)
                         .get_channel(payload.channel_id)
                         .fetch_message(payload.message_id))
        
        await self._xmpp.on_discord_reaction(payload.guild_id,
                                             payload.channel_id,
                                             payload.emoji.name,
                                             message,
                                             payload.user_id,
                                             payload.event_type)
       
    async def on_raw_reaction_add(self, payload):
        await self.on_reaction(payload)

    async def on_raw_reaction_remove(self, payload):
        await self.on_reaction(payload)
