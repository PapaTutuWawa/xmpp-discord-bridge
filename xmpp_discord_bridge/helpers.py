from nextcord import Status

def discord_status_to_xmpp_show(status):
    return {
        Status.online: "available",
        Status.idle: "away",
        Status.dnd: "dnd",
        Status.do_not_disturb: "dnd",
        Status.invisible: "xa", # TODO: Kinda
        Status.offline: "unavailable"
    }.get(status, "available")
