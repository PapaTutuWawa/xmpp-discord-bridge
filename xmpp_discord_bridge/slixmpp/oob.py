from slixmpp.xmlstream import ElementBase

class OOBData(ElementBase):
    """
    XEP-0066 OOB data element for messages
    """
    name = "x"
    namespace = "jabber:x:oob"
    plugin_attrib = "oob"
    interfaces = {"url"}
    sub_interfaces = interfaces
